﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MagicRoyal
{

    public class Utils : MonoBehaviour
    {

        public static float GetAnimationTime(Animator animator, string animationName)
        {
            //Test
            AnimationClip[] animationClipsArray = animator.runtimeAnimatorController.animationClips;
            AnimationClip animationClip = Array.Find(animationClipsArray, (x) => x.name == animationName);
            if (animationClip == null)
            {
                Debug.LogWarning(string.Format("Animation clip {0} not found", animationName));
                return 0f;
            }
            float animTime = animationClip.length;
            return animTime;
        }

        public static Vector3 offset_point(Vector3 pos, float angle, float distance)
        {
            return new Vector3(pos.x + Mathf.Sin(angle) * distance,
                pos.y, pos.z + Mathf.Cos(angle) * distance);
        }

        public static Vector3 offset_point(Vector3 position, Vector3 target, float distance)
        {
            return position + (target - position).normalized * distance;
        }

        public static void DrawindSprite(GameObject obj, int i = 0)
        {
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();

            if (spriteRenderer != null)
            {
                spriteRenderer.sortingOrder = ~(int)(obj.transform.position.y * 100 - (i - 0.9f));
            }

            if (obj.transform.childCount != 0)
                for (int y = 0; y < obj.transform.childCount; y++)
                    DrawindSprite(obj.transform.GetChild(y).gameObject, i);
        }
    }
}
