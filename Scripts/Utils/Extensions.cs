﻿using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class Extensions
    {
        public static T GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static T GetRandom<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        public static T GetRandom<T>(this T[,] array)
        {
            if (array.Length == 0)
                return default(T);

            return array[Random.Range(0, array.GetLength(0)), Random.Range(0, array.GetLength(1))];
        }
    }
}