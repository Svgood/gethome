﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningBoxEvent : MonoBehaviour
{
    public Image image;

    public AudioClip openingClip;
    public AudioClip closingClip;

    public  Switch switcher;

    public bool isOpened = false;

    protected void Start()
    {
        if(switcher)
        {
            switcher.OnTrue += OnTrue;
        }
    }
    private void OnEnable()
    {
        if (isOpened)
        {
            Core.UI.ScaledBox.instance.OnDeactivate += OnDeactivate;
            Core.UI.ScaledBox.instance.OnActivate += OnActivate;
        }
    }

    protected void OnTrue()
    {
        isOpened = true;
        image.gameObject.SetActive(true);
        SoundSystem.SoundManager.instance.PlaySound(openingClip);
        Core.UI.ScaledBox.instance.OnDeactivate += OnDeactivateSwitch;
        Destroy(switcher.gameObject);
    }

    protected void OnDeactivate()
    {
        SoundSystem.SoundManager.instance.PlaySound(closingClip);
        // Core.UI.ScaledBox.instance.OnDeactivate -= OnDeactivate;
    }

    protected void OnDeactivateSwitch()
    {
        SoundSystem.SoundManager.instance.PlaySound(closingClip);
        Core.UI.ScaledBox.instance.OnDeactivate -= OnDeactivateSwitch;
        // Core.UI.ScaledBox.instance.OnDeactivate -= OnDeactivate;
    }

    protected void OnActivate()
    {
        SoundSystem.SoundManager.instance.PlaySound(openingClip);
       // Core.UI.ScaledBox.instance.OnActivate -= OnActivate;
    }

    protected void OnDisable()
    {
        if(isOpened)
        {
            Core.UI.ScaledBox.instance.OnDeactivate -= OnDeactivate;
            Core.UI.ScaledBox.instance.OnActivate -= OnActivate;
        }
    }
}
