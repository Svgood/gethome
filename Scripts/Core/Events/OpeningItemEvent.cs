﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpeningItemEvent : MonoBehaviour
{
    public AudioClip openingClip;
    public AudioClip closingClip;

    private void OnEnable()
    {
        //SoundSystem.SoundManager.instance.PlaySound(openingClip);
        Core.UI.ScaledItem.instance.OnDeactivate += OnDeactivate;
        Core.UI.ScaledItem.instance.OnActivate += OnActivate;
    }


    protected void OnDeactivate()
    {
        SoundSystem.SoundManager.instance.PlaySound(closingClip);
        // Core.UI.ScaledBox.instance.OnDeactivate -= OnDeactivate;
    }

    protected void OnActivate()
    {
        SoundSystem.SoundManager.instance.PlaySound(openingClip);
        // Core.UI.ScaledBox.instance.OnActivate -= OnActivate;
    }

    protected void OnDisable()
    {
        //SoundSystem.SoundManager.instance.PlaySound(closingClip);

        Core.UI.ScaledItem.instance.OnDeactivate -= OnDeactivate;
        Core.UI.ScaledItem.instance.OnActivate -= OnActivate;

    }
}
