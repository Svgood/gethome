﻿using System;
using SoundSystem;
using UnityEngine;

namespace Core.SoundSystem
{
    [Serializable]
    public class Sound  {

        public AudioClipType type;
        public AudioClip audioClip;
    }
}
