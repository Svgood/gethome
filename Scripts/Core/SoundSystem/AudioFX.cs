﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using SoundSystem;
using UnityEngine;

public class AudioFX : MonoBehaviour
{

    public AudioClip audioClip;

    public void Play()
    {
        SoundManager.instance.PlaySound(audioClip);
    }
}
