﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace SoundSystem
{
    public enum AudioClipType
    {
        MainTheme,
        SecondTheme,
        DragTake,
        DragDrop
    }

    

    public class SoundManager : MonoBehaviour
    {
        public static SoundManager instance;

        public List<Sound> preloadedSounds;

        public List<AudioClip> musicClips;

        protected int currentMusic = 0;

        public AudioSource musicSouce;

        protected List<AudioSource> audioSources = new List<AudioSource>();

        protected void Awake()
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
            musicSouce = new GameObject("Music Source").AddComponent<AudioSource>();
            musicSouce.transform.parent = transform;
            //PlayMusic(musicClips[currentMusic]);
        }

        protected void Update()
        {
            if (musicClips.Count != 0 && !musicSouce.isPlaying)
            {
                PlayMusic(musicClips[currentMusic]);
                currentMusic = (currentMusic + 1 >= musicClips.Count) ? 0 : currentMusic + 1;

            }
        }

        public void PlayMusic(AudioClip clip)
        {
            musicSouce.clip = clip;
            musicSouce.Play();
        }

        public AudioSource PlaySound(AudioClip audioClip)
        {
            return SpawnAudioSource(audioClip);
        }

        public AudioSource PlaySound(AudioClipType type)
        {
            var audioClip = preloadedSounds.Find(obj => obj.audioType == type).clip;
            return SpawnAudioSource(audioClip);
        }

        public AudioSource SpawnAudioSource(AudioClip audioClip)
        {
            if (audioClip == null) return null;
            var audioSourceObj = new GameObject("AudioSource").AddComponent<AudioSource>();
            audioSourceObj.transform.parent = transform;
            audioSourceObj.clip = audioClip;
            audioSourceObj.Play();

            audioSources.Add(audioSourceObj);

            for (int i = audioSources.Count - 1; i >= 0; i--)
            {
                if (!audioSources[i].isPlaying)
                {
                    Destroy(audioSources[i].gameObject);
                    audioSources.RemoveAt(i);
                }
            }

            return audioSourceObj;
        }
    }

    [System.Serializable]
    public class Sound
    {
        public string name;
        public AudioClipType audioType;
        public AudioClip clip;
    }
}