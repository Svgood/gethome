﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MessageType
{
    His,
    Her,
    Event,
}

public class Message : MonoBehaviour
{
    public Text text;
    public RectTransform rectTransform;
    public MessageType type = MessageType.His;
}
