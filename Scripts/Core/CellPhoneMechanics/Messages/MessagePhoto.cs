﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;

public class MessagePhoto : Message
{
    public RectTransform scenePlacement;
    public GameScene gameScene;

    private bool isInited = false;

    public void Init(GameScene gameScene)
    {
        this.gameScene = gameScene;
        var obj = Instantiate(gameScene, GameManager.instance.gameCanvas.transform);
        obj.transform.localScale = obj.minScale;
        obj.transform.SetParent(scenePlacement.transform);
        obj.rectTransform.anchoredPosition = scenePlacement.anchoredPosition;
        isInited = true;
    }

    private void Start()
    {
        if (isInited) return;
        Init(GameManager.instance.dataManager.gameScenesLibrary.GetRandomScene());
    }
}
