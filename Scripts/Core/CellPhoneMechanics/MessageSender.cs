﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using Utils;

public class MessageSender : MonoBehaviour
{
    private List<string> randomWords = new List<string>()
    {
        "wdoajwfoajwfoj",
        "Hi motherfucker suka",
        "Ti lox",
        "Sosamba",
        "Maks DEBIL AZAZAZ",
        "AZAAZA",
        "SOSAAAAAAAAAAAAAT",
        "Alaxy akbar",
    };

    public Message hisMessage;
    public Message herMessage;
    public MessagePhoto gameSceneMessage;
    public RectTransform content;

    private Queue<Message> messageQueue = new Queue<Message>();
    private Message createdMessage;
    private int messageLimit = 20;

    private void SendHisMessage(string msg)
    {
        createdMessage = Instantiate(hisMessage, content);

        SendMessage(msg);
    }

    private void SendHerMessage(string msg)
    {
        createdMessage = Instantiate(herMessage, content);
        SendMessage(msg);
    }

    private void SendMessage(string msg)
    {
        createdMessage.text.text = msg;
        createdMessage.rectTransform.sizeDelta = new Vector2(createdMessage.rectTransform.sizeDelta.x, createdMessage.text.preferredHeight + 40);
        messageQueue.Enqueue(createdMessage);
        if (messageQueue.Count > messageLimit)
        {
            Destroy(messageQueue.Dequeue().gameObject);
        }
    }

    public void SendGameSceneMessage(GameScene gameScene)
    {
        var obj = Instantiate(gameSceneMessage, content);
        obj.Init(gameScene);
        //messageQueue.Enqueue(obj);
        //if (messageQueue.Count > messageLimit)
        //{
        //    Destroy(messageQueue.Dequeue().gameObject);
        //}
    }

    //public void SendRandomMessage()
    //{
    //    int rand = Random.Range(0, 2);
    //    if (rand == 0) SendHerMessage(randomWords.GetRandom());
    //    if (rand == 1) SendHisMessage(randomWords.GetRandom());
    //}

    public void SendMessage(MessageType type, string msg)
    {
        switch (type)
        {
            case MessageType.His:
                SendHisMessage(msg);
                break;
            case MessageType.Her:
                SendHerMessage(msg);
                break;
            case MessageType.Event:
                SendGameSceneMessage(GameManager.instance.dataManager.gameScenesLibrary.gameScenes[GameManager.instance.currentScene]);
                GameManager.instance.currentScene++;
                if (GameManager.instance.currentScene >=
                    GameManager.instance.dataManager.gameScenesLibrary.gameScenes.Count)
                    GameManager.instance.currentScene = 0;
                break;
        }
    }
}
