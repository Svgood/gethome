﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;

public class CellPhone : MonoBehaviour
{
    public RectTransform rectTransform;
    public ScrollRect scrollRect;
    public MessageSender messageSender;

    public bool renderEventMessages = true;

    void Start()
    {
        GameManager.instance.AddCellPhone(this);
    }
    // Update is called once per frame
    void Update()
    {
        //scrollRect.vertical = !GameManager.instance.gameSceneManager.IsSceneActive;
        scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
    }

    public void SendMessage(MessageType type, string msg)
    {
        if (type == MessageType.Event && !renderEventMessages) return;
        messageSender.SendMessage(type, msg);
        scrollRect.velocity = new Vector2(0, 600);
    }
}
