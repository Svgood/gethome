﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Core
{
    public class MoneyManager : MonoBehaviour
    {

        public float money = 50;
        public TextMeshProUGUI moneyText;

        private void Start()
        {
            moneyText.text = money.ToString("0");
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    ChangeMoney(Random.Range(100, 200));
            //}
        }

        public void ChangeMoney(float value)
        {
            StopAllCoroutines();
            StartCoroutine(ChangingPercent(money + value));
        }

        protected IEnumerator ChangingPercent(float value)
        {
            while ((int)money != (int)value)
            {
                money = Mathf.Lerp(money, value, 10 * Time.deltaTime);
                moneyText.text = $"{money.ToString("0")}";
                yield return new WaitForEndOfFrame();
            }

        }
    }
}
