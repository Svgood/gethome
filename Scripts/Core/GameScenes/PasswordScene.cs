﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class PasswordScene : GameScene
{

    public InputField inputField;

    private int password = 415;

    protected override void Start()
    {
        base.Start();
        inputField.readOnly = true;
    }

    public void ButtonNumPressed(int num)
    {
        if (completed) return;
        GameManager.instance.SendMessage(MessageType.His, $"Нажми {num}");
        inputField.textComponent.color = Color.black;
        inputField.text += num;
    }

    public void ButtonXPressed()
    {
        if (completed) return;
        GameManager.instance.SendMessage(MessageType.His, $"Сотри всё, мы ошиблись.");
        inputField.text = "";
    }

    public void ButtonEnterPressed()
    {
        if (completed) return;
        GameManager.instance.SendMessage(MessageType.His, $"Принимай и вводи.");
        if (inputField.text.Length == 0)
        {
            GameManager.instance.SendMessage(MessageType.Her, $"Но тут же пусто!");
            return;
        }
        if (int.Parse(inputField.text) == password)
        {
            inputField.textComponent.color = Color.green;
            StartCoroutine(WinCoroutine());
            return;
        }

        GameManager.instance.messageManager.SendMessageWithTyping(MessageType.Her, failurePhrases.GetRandom());
        inputField.textComponent.color = Color.red;
    }
}
