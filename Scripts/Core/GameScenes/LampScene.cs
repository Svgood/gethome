﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class LampScene : GameScene
{
    public List<Button> buttons = new List<Button>();

    public List<string> angryNoises = new List<string>();

    private List<int> winCoditionButtons = new List<int>()
    {
        0,1,2,3
    };
    private List<int> winCoditionColors= new List<int>()
    {
        0,1,2,0
    };

    private List<Color> colors = new List<Color>()
    {
        Color.red,
        Color.blue,
        Color.yellow
    };

    private bool won = false;
    private int tryies = 0;

    protected override void Start()
    {
        base.Start();
        foreach (var button in buttons)
        {
            var colorBlock = button.colors;

            var color = colors.GetRandom();
            colorBlock.normalColor = color;
            color.a = 0.5f;
            colorBlock.highlightedColor = color; 
            button.colors = colorBlock;
        }
    }

    public void ButtonPressed(int id)
    {
        if (won) return;

        tryies++;
        GameManager.instance.SendMessage(MessageType.His, $"Нажми на {TextFromId(id)}.");
        var index = colors.FindIndex(x => x == buttons[id].colors.normalColor);
        var colorBlock = buttons[id].colors;

        index++;
        if (index >= colors.Count) index = 0;

        var color = colors[index];
        colorBlock.normalColor = color;
        color.a = 0.5f;
        colorBlock.highlightedColor = color; 
        buttons[id].colors = colorBlock;

        if (CheckWin())
        {
            won = true;
            Win();
            return;
        }

        if (tryies > 16)
        {
            GameManager.instance.messageManager.SendMessageWithTyping(MessageType.Her, angryNoises.GetRandom());
        }
    }

    private bool CheckWin()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            for (int j = 0; j < winCoditionButtons.Count; j++)
            {
                if (winCoditionButtons[j] == i && colors[winCoditionColors[j]] != buttons[i].colors.normalColor)
                    return false;
            }
        }

        return true;
    }

    public override void Win()
    {
        foreach (var button in buttons)
        {
            var colorBlock = button.colors;
            var color = Color.green;
            colorBlock.normalColor = color;
            color.a = 0.5f;
            colorBlock.highlightedColor = color;
            button.colors = colorBlock;
        }

        StartCoroutine(WinCoroutine());
    }

    protected string TextFromId(int id)
    {
        if (id == 0) return "верхнюю левую";
        if (id == 1) return "верхнюю посередине";
        if (id == 2) return "верхнюю правую";
        if (id == 3) return "среднюю слева";
        if (id == 4) return "среднюю левую";
        if (id == 5) return "среднюю левую";
        if (id == 6) return "нижнюю левую";
        if (id == 7) return "нижнюю середину";
        if (id == 8) return "нижнюю правую";
        return "...хмм";
;    }
}
