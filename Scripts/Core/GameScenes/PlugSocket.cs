﻿using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlugSocket : DropTarget
{
    public DualScene2 plugScene;
    public string id;

    protected override void Start()
    {
        base.Start();
        id = transform.GetChild(0).GetComponent<Text>().text;
    }

    public void AddRightPlug()
    {
        plugScene.EnablePlug();
    }

}
