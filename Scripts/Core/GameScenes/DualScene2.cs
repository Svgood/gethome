﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;

public class DualScene2 : GameScene
{
    public List<Image> powerImages = new List<Image>();
    private int enabledPlugs = 0;

    public void EnablePlug()
    {
        enabledPlugs++;
        for (int i = 0; i < enabledPlugs; i++)
        {
            powerImages[i].color = Color.green;
        }

        if (enabledPlugs == 4)
        {
            GameManager.instance.enabledPowerBlock = true;
            Win();
        }
    }

    public override void Win()
    {
        enabledPlugs = 4;
        for (int i = 0; i < enabledPlugs; i++)
        {
            powerImages[i].color = Color.green;
        }
        GameManager.instance.enabledPowerBlock = true;
        base.Win();
    }

    public override void Completed()
    {
        Toggle();
    }

    public void DisablePlug()
    {
        enabledPlugs--;
    }
}
