﻿using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneObject : Draggable
{
    public Image image;

    public override void Snap(DropTarget target)
    {
        if (target is GameScene)
        {
            SetParent(target.transform);
        }
    }
}
