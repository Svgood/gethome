﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;

public class DualScene1 : GameScene
{
    public Image energyBlock;
    public Image doorLocker;

    public List<Image> doorsClosed = new List<Image>();
    public List<Image> doorsOpened = new List<Image>();

    private bool switched = false;

    void Update()
    {
        if (GameManager.instance.enabledPowerBlock && !switched)
        {
            switched = true;
            energyBlock.color = Color.green;
            doorLocker.color = Color.green;

            foreach (var door in doorsClosed)
            {
                door.enabled = false;
            }

            foreach (var door in doorsOpened)
            {
                door.enabled = true;
            }
        }
    }
}
