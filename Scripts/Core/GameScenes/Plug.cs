﻿using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Plug : GameSceneObject
{
    public Text text;

    public string id;
    public string hiddenId;

    protected override void Start()
    {
        base.Start();
        text.text = id.ToString();
        text.enabled = false;
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        outline.enabled = true;
        text.enabled = true;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        outline.enabled = false;
        text.enabled = false;
    }

    public override void Snap(DropTarget dropTarget)
    {
        if (dropTarget is PlugSocket)
        {
            var plugSocket = dropTarget as PlugSocket;
            SetParent(plugSocket.transform);
            rectTransform.anchoredPosition = Vector2.zero;
            if (hiddenId == plugSocket.id)
            {
                Debug.Log("Xyi");
                plugSocket.AddRightPlug();
                Destroy(gameObject);
                Destroy(plugSocket.gameObject);

            }
            return;
        }

        ReturnToLastParent();
    }
}
