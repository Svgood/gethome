﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Core.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;

public class GameScene : DropTarget, IPointerClickHandler
{
    public float sceneMessageInterval = 3;
    public float completeDurationWait = 2;

    public Vector3 minScale = new Vector3(0.2f, 0.2f, 0.2f);

    public event Action OnSceneCompleted;

    public List<string> succesPhrases = new List<string>();
    public List<string> failurePhrases = new List<string>();
    public List<string> dialogMessages = new List<string>();
    public List<string> trashTalk = new List<string>();

    private int messagePointer = -1;

    private bool isRolled = true;
    protected bool completed = false;

    //private bool IsAtCenter => Mathf.Approximately(rectTransform.anchoredPosition.x, 0) &&
    //                           Mathf.Approximately(rectTransform.anchoredPosition.y, 0);

    private bool IsAtCenter => Vector3.Distance(rectTransform.anchoredPosition, Vector2.zero) < 2;

    //private bool IsAtLastPosition => Mathf.Approximately(transform.position.x, lastParent.transform.position.x) &&
    //                                 Mathf.Approximately(transform.position.y, lastParent.transform.position.y);

    private bool IsAtLastPosition => Vector3.Distance(lastParent.transform.position, transform.position) < 4;

    private bool IsScaledAtMax => Mathf.Approximately(rectTransform.localScale.x, 1) && Mathf.Approximately(rectTransform.localScale.y, 1);
    //private bool IsScaledAtMin=> Mathf.Approximately(rectTransform.localScale.x, minScale.x) && Mathf.Approximately(rectTransform.localScale.y , minScale.y);
    private bool IsScaledAtMin => Mathf.Abs(rectTransform.localScale.magnitude - minScale.magnitude) < 0.2f;

    private Vector3 lastPosition;

    private GameSceneManager GameSceneManager => GameManager.instance.gameSceneManager;

    private Transform lastParent;

    protected override void Start()
    {
        base.Start();
        lastPosition = transform.position;
        lastParent = transform.parent;
    }

    private void Update()
    {
        if (isRolled)
        {
            lastPosition = transform.position;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (completed) return;
        if (!isRolled) return;
        Toggle();
    }

    public void Toggle()
    {
        if (transform.parent != GameManager.instance.gameCanvas.transform)
            lastParent = transform.parent;
        StopAllCoroutines();
        StartCoroutine(Roll());
    }

    public void ButtonToggle()
    {
        if (completed) return;
        Toggle();
    }

    private IEnumerator Roll()
    {
        Debug.Log("Started rolling");
        if (isRolled)
        {
            transform.SetParent(GameManager.instance.gameCanvas.transform);
            isRolled = false;
            GameSceneManager.SetScene(this);

            GameSceneManager.OnSceneStartedOpening?.Invoke();

            while (!IsAtCenter || !IsScaledAtMax)
            {
                rectTransform.localScale = Vector3.Lerp(rectTransform.localScale, Vector3.one, 4 * Time.deltaTime);
                rectTransform.anchoredPosition = Vector3.Lerp(rectTransform.anchoredPosition, Vector3.zero, 4 * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            GameSceneManager.OnSceneStartedClosing?.Invoke();

            while (!IsAtLastPosition || !IsScaledAtMin)
            {
                rectTransform.localScale = Vector3.Lerp(rectTransform.localScale, minScale, 10 * Time.deltaTime);
                rectTransform.transform.position = Vector3.Lerp(rectTransform.transform.position, lastParent.transform.position, 10 * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            rectTransform.transform.position = lastParent.transform.position;
            rectTransform.localScale = minScale;
            GameSceneManager.CloseScene();
            isRolled = true;
            transform.SetParent(lastParent);
        }
    }

    public string GetMessage()
    {
        messagePointer++;
        if (messagePointer < dialogMessages.Count)
        {
            return dialogMessages[messagePointer];
        }
        else
        {
            messagePointer = -1;
            dialogMessages = trashTalk;
            return "...";
        }
    }

    public virtual void Completed()
    {
        OnSceneCompleted?.Invoke();
        if (!isRolled)
        {
            Toggle();
        }
    }

    public virtual void Lost()
    {
        OnSceneCompleted?.Invoke();
        Toggle();
    }

    public virtual void Win()
    {
        StartCoroutine(WinCoroutine());
    }

    protected IEnumerator WinCoroutine()
    {
        completed = true;
        GameManager.instance.messageManager.SendMessageWithTyping(MessageType.Her, succesPhrases.GetRandom());
        GameManager.instance.messageManager.Wait(completeDurationWait + 1);
        yield return new WaitForSeconds(completeDurationWait);
        Completed();
    }

    protected IEnumerator LoseCoroutine()
    {
        completed = true;
        GameManager.instance.messageManager.SendMessageWithTyping(MessageType.Her, failurePhrases.GetRandom());
        GameManager.instance.messageManager.Wait(completeDurationWait + 1);
        yield return new WaitForSeconds(completeDurationWait);
        Lost();
    }

}
