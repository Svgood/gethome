﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;


[CreateAssetMenu(fileName = "ScenesLibrary", menuName = "Scenes Library")]
public class GameScenesLibrary : ScriptableObject
{
    public List<GameScene> gameScenes = new List<GameScene>();

    public GameScene GetRandomScene() => gameScenes.GetRandom();
}
