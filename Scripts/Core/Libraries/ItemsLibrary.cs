﻿using System.Collections.Generic;
using Core.Items;
using UnityEngine;

namespace Core.Libraries
{
    [CreateAssetMenu(fileName = "ItemsLibrary", menuName = "Items Library")]
    public class ItemsLibrary : ScriptableObject {

        public List<Item> items = new List<Item>();

        //public Item GetItem(ItemType type) => items.Find(x => x.type == type);
    }
}
