﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Core.Items;
using UnityEngine.UI;
using Core.UI;
using Core.BackgroundElements;

namespace Core.Items
{
    public class Box : DropTarget, IPointerClickHandler
    {
        public Image image;
        public ItemContent contentPrefab;

        [HideInInspector]
        public ItemContent currentContent;

        public Outline outline;

        protected override void Start()
        {
            base.Start();
            currentContent = Instantiate(contentPrefab, ScaledBox.instance.transform);
            currentContent.gameObject.SetActive(false);
            outline.enabled = false;
            //22currentContent.transform.position = new Vector2(Screen.width / 2, Screen.height / 2);

            //currentContent.transform/
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if(eventData.button == PointerEventData.InputButton.Left)
            {
                ScaledBox.instance.Activate(this);
            }
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            outline.enabled = true;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            outline.enabled = false;
        }
    }
}