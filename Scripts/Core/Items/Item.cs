﻿using System.Collections;
using System.Collections.Generic;
using Core.BackgroundElements;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;
using Core.UI;

namespace Core.Items
{
    public enum ItemType
    {
        None,
        Dynamit,
        Crowbar,
        Gun,
        Lockpick,
        Chlorophorm,
        Disguise,
        Photo,
        Car,
    }

    public class Item : Draggable
    {
        [HideInInspector]
        public Image image;

        public ItemContent contentPrefab;

        public int durability = 2;

        public bool isActive = true;

        protected bool used = false;

        public bool useImageForContent = false;

        protected override void Start ()
        {
            base.Start();
            if (outline == null)
                outline = GetComponent<Outline>();
            outline.enabled = false;
            image = GetComponent<Image>();

            //cost = Random.Range(10, 200);
        }

        public override void Snap(DropTarget target)
        {
            if (used)
                return;

            if (target is Table)
            {
                Debug.Log("Snap to Table");
                SetParent(target.transform);
                return;
            }
            if(target is TrashCan)
            {
                Destroy(gameObject);
                return;
            }
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            StopAllCoroutines();
            base.OnBeginDrag(eventData);
        }

        public void ThrowToPoint(Vector3 point)
        {
            StartCoroutine(Throwing(point));
        }

        protected IEnumerator Throwing(Vector3 point)
        {
            while (Vector3.Distance(transform.position, point) > 0.5f)
            {
                transform.position = Vector3.Lerp(transform.position, point, 3f * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            if (!eventData.dragging && eventData.button == PointerEventData.InputButton.Left)
            {
                ScaledItem.instance.Activate(this, useImageForContent);
            }
        }
    }
}
