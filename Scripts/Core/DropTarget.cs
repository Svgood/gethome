﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Core
{
    public class DropTarget : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public RectTransform rectTransform;

        protected virtual void Start()
        {
            //rectTransform = GetComponent<RectTransform>();
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {

        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {

        }
    }
}
