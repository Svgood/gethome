﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;

namespace Core.BackgroundElements
{
    public class ShelfDrag : Draggable
    {

        protected Vector3 startingPosition;
        public float yLimit = 150;

        public event Action OnShelfClosed;

        protected override void Start()
        {
            base.Start();
            startingPosition = transform.position;
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            dragged = true;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (transform.position.y + eventData.delta.y > startingPosition.y - yLimit /** Adaptivity.ScaleCoefficient*/)
            {
                transform.position += Vector3.up * eventData.delta.y;
            }
            else
            {
                transform.position = startingPosition + Vector3.down * yLimit /** Adaptivity.ScaleCoefficient*/;
                return;
            }

            //if (transform.position.y + eventData.delta.y < startingPosition.y)
            //    transform.position += Vector3.up * eventData.delta.y;
            if (transform.position.y + eventData.delta.y > startingPosition.y)
            {
                transform.position = startingPosition;
                OnShelfClosed?.Invoke();
            }
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            dragged = false;
        }
    }
}
