﻿using UnityEngine;
using Utils;

namespace Core.BackgroundElements
{
    public class Table : DropTarget
    {

        public RectTransform itemLandZone;

        public Vector3 GetRandomPointInside()
        {
            Vector3 pointInside = new Vector3(
                Random.Range(itemLandZone.position.x - itemLandZone.rect.width / 2 /** Adaptivity.ScaleCoefficient*/, itemLandZone.position.x + itemLandZone.rect.width / 2/* * Adaptivity.ScaleCoefficient*/),
                Random.Range(itemLandZone.position.y - itemLandZone.rect.height / 2 /** Adaptivity.ScaleCoefficient*/, itemLandZone.position.y + itemLandZone.rect.height / 2 /** Adaptivity.ScaleCoefficient*/),
                0
               );

            return pointInside;
        }

    }
}
