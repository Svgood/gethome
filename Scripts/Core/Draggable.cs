﻿using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Core.BackgroundElements;
using SoundSystem;

namespace Core
{
    public class Draggable : MonoBehaviour, IDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IEndDragHandler
    {
        public bool smoothReturn = false;
        public bool dragged;
        public bool canDrag = true;

        public Outline outline;

        protected Transform lastParent;
        protected RectTransform rectTransform;
        protected Vector3 lastPosition;
        protected Vector2 offset;

        protected List<RaycastResult> raycastTargets = new List<RaycastResult>();

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            if (!canDrag) return;
            dragged = true;
            lastPosition = transform.position;
            offset = new Vector2(transform.position.x, transform.position.y) - eventData.position;
            UnParent();
            SoundManager.instance.PlaySound(AudioClipType.DragTake);
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            if (!canDrag) return;
            transform.position = eventData.position /*+ offset*/;
            //Fix
            if (eventData.position.x > Screen.width || eventData.position.y > Screen.height || eventData.position.x < 0 || eventData.position.y < 0)
            {
                ReturnToLastParent();
                dragged = false;
            }
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            if (!canDrag) return;
            EventSystem.current.RaycastAll(eventData, raycastTargets);
            foreach (var target in raycastTargets)
            {
                DropTarget dropTarget = target.gameObject.GetComponent<DropTarget>();
                if (dropTarget == null) continue;
                if (dropTarget is NotDropable) break;
                Snap(dropTarget);
                SoundManager.instance.PlaySound(AudioClipType.DragDrop);
                break;
            }

            dragged = false;
            ReturnToLastParent();
        }

        public virtual void Snap(DropTarget target)
        {
            SetParent(target.transform);
        }

        public void SetParent(Transform target)
        {
            lastParent = target.transform;
            lastPosition = transform.position;
        }

        public void UnParent()
        {
            transform.SetParent(GameManager.instance.gameCanvas.transform);
        }

        public void ReturnToLastParent()
        {
            ReturnToLastPosition();
            transform.SetParent(lastParent);
        }

        public void ReturnToLastPosition()
        {
            if (smoothReturn) return;
            transform.position = lastPosition;
        }

        protected void SmoothReturning()
        {
            if (!smoothReturn) return;
            if (dragged) return;

            transform.position = Vector3.Lerp(transform.position, lastPosition, 10 * Time.deltaTime);
        }

        // Use this for initialization
        protected virtual void Start ()
        {
            if (lastParent == null)
            {
                SetParent(transform.parent);
            }

            rectTransform = GetComponent<RectTransform>();
        }

        // Update is called once per frame
        protected virtual void Update () {
            SmoothReturning();
        }


        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            outline.enabled = true;
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            outline.enabled = false;
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            
        }
    }
}
