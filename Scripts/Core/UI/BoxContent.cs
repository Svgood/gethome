﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Items;


namespace Core.UI
{
    public class BoxContent : ItemContent
    {
        protected List<Item> contentObjects;

        protected override void Awake()
        {
            base.Awake();
            //OnImageIncreased += ActivateObjects;
           // contentObjects = new List<Item>( GetComponentsInChildren<Items.Item>());
        }

        protected void ActivateObjects()
        {
            foreach (Item item in contentObjects)
            {
                item.gameObject.SetActive(true);
            }
        }

        protected void DeactivateObjects()
        {
            foreach (Item item in contentObjects)
            {
                item.gameObject.SetActive(false);
            }
        }

        public override void Deactivate()
        {
            base.Deactivate();
          //  DeactivateObjects();
        }

        protected void OnEnable()
        {
            //DeactivateObjects();
        }
    }

}
