﻿using Core.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class ScaledItem : MonoBehaviour
    {
        public static ScaledItem instance;

        public Image background;
        public ItemContent currentContent;

        protected bool isActive = false;

        public Action OnDeactivate;
        public Action OnActivate;

        protected Vector2 normalSize;

        protected virtual void Awake()
        {
            instance = this;
        }

        public virtual void Activate(Item item, bool setSprite = false)
        {
            if(!isActive)
            {
                background.gameObject.SetActive(true);
                currentContent = Instantiate(item.contentPrefab, transform);
                currentContent.OnImageIncreased += OnImageIncreased;
                currentContent.OnImageDecreased += OnImageDecreased;
                currentContent.SetPicture(item.image, setSprite);
                OnActivate?.Invoke();
                currentContent.Activate();
            }
        }

        protected void OnImageIncreased()
        {
            isActive = true;
        }

        protected void OnImageDecreased()
        {
            isActive = false;
            background.gameObject.SetActive(false);
            Destroy(currentContent.gameObject);
            currentContent = null;
        }

        

        public virtual void Deactivate()
        {
            if (isActive)
            {
                OnDeactivate?.Invoke();
                currentContent.Deactivate();
                
            }
        }
    }

}