﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class ComplexItemContent : ItemContent
    {
        public List<Sprite> sprites;

        protected int currentPage = 0;

        public Button prevPageBtn;
        public Button nextPageBtn;

        protected override void Awake()
        {
            base.Awake();
            image.sprite = sprites[currentPage];
            prevPageBtn.enabled = false;
        }

        public void SwitchPage(bool isNext)
        {
            currentPage = isNext ? currentPage + 1 : currentPage - 1;
            image.sprite = sprites[currentPage];

            nextPageBtn.enabled = currentPage < sprites.Count - 1;
            prevPageBtn.enabled = currentPage > 0;
        }
    }

}