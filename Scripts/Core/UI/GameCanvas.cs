﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;

public class GameCanvas : MonoBehaviour
{ 
    public Animator animator;
    public GameSceneManager GameSceneManager => GameManager.instance.gameSceneManager;
    public CellPhone gameSceneCellPhone;

    private Vector2 targetPosition;
    private Vector2 startingPosition;

    void Start()
    {
        targetPosition = new Vector2(gameSceneCellPhone.rectTransform.anchoredPosition.x, -100);
        startingPosition = gameSceneCellPhone.rectTransform.anchoredPosition;

        GameSceneManager.OnSceneStartedOpening += OpenGameSceneCellPhone;
        GameSceneManager.OnSceneStartedClosing += CloseGameSceneCellPhone;
    }

    public void OpenGameSceneCellPhone()
    {
        StopAllCoroutines();
        StartCoroutine(StartOpening());
    }

    public void CloseGameSceneCellPhone()
    {
        StopAllCoroutines();
        StartCoroutine(StartClosing());
        //animator.ResetTrigger("Open");
        //animator.SetTrigger("Close");
    }

    IEnumerator StartOpening()
    {
        yield return new WaitForSeconds(0.5f);
        gameSceneCellPhone.transform.SetParent(null);
        gameSceneCellPhone.transform.SetParent(transform);
        gameSceneCellPhone.rectTransform.anchoredPosition = startingPosition;

        var lastTime = Time.time;
        while (Time.time - lastTime < 2)
        {
            gameSceneCellPhone.rectTransform.anchoredPosition =
                Vector2.Lerp(gameSceneCellPhone.rectTransform.anchoredPosition, targetPosition, 6 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        //animator.ResetTrigger("Close");
        //animator.SetTrigger("Open");
    }

    IEnumerator StartClosing()
    {
        var lastTime = Time.time;
        while (Time.time - lastTime < 2)
        {
            gameSceneCellPhone.rectTransform.anchoredPosition =
                Vector2.Lerp(gameSceneCellPhone.rectTransform.anchoredPosition, startingPosition, 6 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
