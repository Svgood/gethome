﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.UI
{
    public class ItemContent : MonoBehaviour
    {
        public Image image;

        protected Vector2 scaledSize;
        protected Vector2 iconSize;

        protected Vector2 iconPos;

        public event Action OnImageIncreased;
        public event Action OnImageDecreased;

        public float lerpSpeed = 10f;

        protected virtual void Awake()
        {
            scaledSize = image.rectTransform.sizeDelta;
            image.gameObject.SetActive(false);
        }

        public void SetPicture(Image newImage, bool setSprite = false)
        {
            if(setSprite)
            {
                image.sprite = newImage.sprite;
            }
            iconSize = newImage.rectTransform.sizeDelta;
            iconPos = newImage.rectTransform.position;

            image.rectTransform.sizeDelta = iconSize;
            image.rectTransform.position = iconPos;
        }

        public void Activate()
        {
            image.gameObject.SetActive(true);
            StartCoroutine(Increase());
        }

        public virtual void Deactivate()
        {
            StartCoroutine(Decrease());
        }

        protected IEnumerator Increase()
        {
            while (image.rectTransform.sizeDelta.y < scaledSize.y - 5)
            {
                image.rectTransform.sizeDelta = Vector3.Lerp(image.rectTransform.sizeDelta, scaledSize, lerpSpeed * Time.deltaTime);
                image.rectTransform.position = Vector3.Lerp(image.rectTransform.position, new Vector2(Screen.width / 2, Screen.height / 2), lerpSpeed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
            OnImageIncreased?.Invoke();
        }

        protected IEnumerator Decrease()
        {
            while (image.rectTransform.sizeDelta.y > iconSize.y + 10)
            {
                image.rectTransform.sizeDelta = Vector3.Lerp(image.rectTransform.sizeDelta, iconSize, lerpSpeed * Time.deltaTime);
                image.rectTransform.position = Vector3.Lerp(image.rectTransform.position, iconPos, lerpSpeed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
            OnImageDecreased?.Invoke();
        }
    }
}


