﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Core.Items;
using Core.BackgroundElements;
using System;

namespace Core.UI
{
    public class ScaledBox : MonoBehaviour
    {
        public static ScaledBox instance;

        public Image background;
        protected ItemContent currentContent;

        protected Box currentBox;

        protected bool isActive = false;

        public Action OnDeactivate;
        public Action OnActivate;

        protected void Awake()
        {
            instance = this;
        }

        public void Activate(Box box)
        {
            if (!isActive)
            {
                background.gameObject.SetActive(true);
                currentContent = box.currentContent.GetComponent<ItemContent>();
                currentContent.transform.parent = transform;
                //currentContent = Instantiate(item.contentPrefab, transform);
                currentContent.gameObject.SetActive(true);
                currentContent.OnImageIncreased += OnImageIncreased;
                currentContent.OnImageDecreased += OnImageDecreased;
                currentContent.SetPicture(box.image);
                OnActivate?.Invoke();
                currentContent.Activate();
            }
        }

        protected void OnImageIncreased()
        {
            isActive = true;
        }

        protected void OnImageDecreased()
        {
            isActive = false;
            background.gameObject.SetActive(false);
            currentContent.gameObject.SetActive(false);
            currentContent.OnImageIncreased -= OnImageIncreased;
            currentContent.OnImageDecreased -= OnImageDecreased;
            //Destroy(currentContent.gameObject);
            currentContent = null;
        }

        //currentBox = box;
        //background.gameObject.SetActive(true);
        //isActive = true;
        //currentContent = box.currentContent;
        //currentContent.gameObject.SetActive(true);
        //currentContent.transform.parent = transform;
        //OnActivate?.Invoke();

        public virtual void Deactivate()
        {
            if (isActive)
            {
                currentContent.Deactivate();
                OnDeactivate?.Invoke();
            }
        }
    }


    //public void Deactivate()
    //    {
    //        if (isActive)
    //        {
    //            currentContent.gameObject.SetActive(false);
    //            currentContent.transform.parent = currentBox.transform;
    //            currentContent = null;
    //            currentBox = null;
    //            background.gameObject.SetActive(false);
    //            isActive = false;
    //            OnDeactivate?.Invoke();
    //        }
    //    }
    //}

}

