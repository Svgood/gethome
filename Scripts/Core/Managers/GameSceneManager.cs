﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    private GameScene currentActiveScene;

    public Action OnSceneStartedOpening;
    public Action OnSceneStartedClosing;
    public Action OnSceneCompleted;

    public bool IsSceneActive => currentActiveScene != null;
    public GameScene CurrentActiveScene => currentActiveScene;

    public void SetScene(GameScene scene)
    {
        currentActiveScene = scene;
        currentActiveScene.OnSceneCompleted += OnSceneCompleted;
    }

    public void CloseScene()
    {
        currentActiveScene.OnSceneCompleted -= OnSceneCompleted;
        currentActiveScene = null;
    }
}
