﻿using System.Collections;
using System.Collections.Generic;
using Core.Libraries;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public ItemsLibrary itemsLibrary;
    public GameScenesLibrary gameScenesLibrary;
    public DialogLibrary dialogLibrary;
}
