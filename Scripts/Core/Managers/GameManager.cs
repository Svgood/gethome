﻿using System;
using System.Collections.Generic;
using Core.BackgroundElements;
using Core.Items;
using Core.Libraries;
using SoundSystem;
using UnityEngine;

namespace Core.Managers
{
    public class GameManager : MonoBehaviour
    {

        public static GameManager instance;
         
        public Canvas gameCanvas; // Был GameCanvas зачем???
        public GameSceneManager gameSceneManager;
        public DataManager dataManager;
        public MessageManager messageManager;
        public CursorManager cursorManager;
        public PlayerInputManager playerInputManager;
        public EpilogueManager epilogueManager;
        public RectTransform epilogue;

        public bool isGameStarted = false;
        public bool enabledPowerBlock = false;
        public int currentScene = 0;

        private List<CellPhone> cellPhones = new List<CellPhone>();
            
        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            //cellPhones = new List<CellPhone>(FindObjectsOfType<CellPhone>());
        }

        public void SendMessage(MessageType type, string msg)
        {
            foreach (var cellPhone in cellPhones)
            {
                cellPhone.SendMessage(type, msg);
            }

        }

        public void AddCellPhone(CellPhone cellPhone)
        {
            cellPhones.Add(cellPhone);
        }

        public void EndGame()
        {
            epilogueManager.StartEpilogue();
        }
    }
}
