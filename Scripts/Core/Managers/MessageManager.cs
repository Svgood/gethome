﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class MessageManager : MonoBehaviour
{
    public DialogLibrary dialogLibrary;

    public float MessageInterval => GameManager.instance.gameSceneManager.IsSceneActive
        ? GameManager.instance.gameSceneManager.CurrentActiveScene.sceneMessageInterval
        : messageInterval;

    public Text typingInfo;

    //Message stat
    private int messagePointer = 0;
    private float messageInterval = 3;
    private float lastTime = 0;

    private bool customDuration;
    private float customDurationTime;

    //For typing info
    private int currentTypingState;
    private List<string> typingState = new List<string>()
    {
        "Eva is typing.",
        "Eva is typing..",
        "Eva is typing...",
    };

    //Working with events
    private bool blockMessages = false;
    private int messagesLeftTillBlock = 0;

    private Coroutine typingCoroutine;

    private void Start()
    {
        dialogLibrary = new DialogLibrary();
        GameManager.instance.gameSceneManager.OnSceneCompleted += Continue;
        lastTime = Time.time;
    }

    private void Update()
    {
        if (!GameManager.instance.isGameStarted) return;
        if (GameManager.instance.playerInputManager.canType) return;

        if (customDuration)
        {
            if (Time.time - lastTime < customDurationTime)
            {
                return;
            }

            customDuration = false;
        }

        DialogListProcessing();
    }

    private void DialogListProcessing()
    {
        if (Time.time - lastTime < MessageInterval)
        {
            return;
        }

        StopAllCoroutines();
        typingCoroutine = null;

        lastTime = Time.time;

        //Scene reaction
        if (GameManager.instance.gameSceneManager.IsSceneActive && blockMessages && messagesLeftTillBlock <= 0)
        {
            StartCoroutine(SendMessageWithTypingCoroutine(MessageType.Her,
                GameManager.instance.gameSceneManager.CurrentActiveScene.GetMessage()));
            return;
        }

        if (blockMessages && messagesLeftTillBlock <= 0) return;

        //Standart workflow
        messagesLeftTillBlock--;
        var sentence = dialogLibrary.dialogList[messagePointer];
        messagePointer++;
        if (messagePointer >= dialogLibrary.dialogList.Count)
        {
            messagePointer = 0;
            GameManager.instance.EndGame();
            return;
        }

        var type = GetType(sentence[0]);

        if (type == MessageType.His)
        {
            GameManager.instance.playerInputManager.LoadSentence(sentence.Substring(1));
            GameManager.instance.playerInputManager.inputField.text = "Waiting for input...";
            StartCoroutine(WaitingResponse());
            return;
        }

        if (type == MessageType.Event)
        {
            blockMessages = true;
            messagesLeftTillBlock = int.Parse(sentence.Substring(1).ToString());
        }

        StartCoroutine(SendMessageWithTypingCoroutine(type, sentence.Substring(1)));
    }

    public void Wait(float duration)
    {
        lastTime = Time.time;
        customDuration = true;
        customDurationTime = duration;
    }

    private MessageType GetType(char symbol)
    {
        if (symbol == '@') return MessageType.Her;
        if (symbol == '!') return MessageType.His;
        if (symbol == '#') return MessageType.Event;
        return MessageType.Her;
    }

    private IEnumerator SendMessageWithTypingCoroutine(MessageType type, string msg)
    {
        yield return new WaitForSeconds(0.1f);
        var startingTime = Time.time;
        while (Time.time - startingTime < 1)
        {
            typingInfo.text = typingState[currentTypingState];
            currentTypingState++;
            if (currentTypingState >= typingState.Count) currentTypingState = 0;
            yield return new WaitForSeconds(0.2f);
        }
        typingInfo.text = "Online";
        GameManager.instance.SendMessage(type, msg);
    }

    public void SendMessageWithTyping(MessageType type, string msg)
    {
        StartCoroutine(SendMessageWithTypingCoroutine(type, msg));
    }

    private IEnumerator WaitingResponse()
    {
        yield return new WaitForSeconds(60f);
        typingCoroutine = StartCoroutine(SendMessageWithTypingCoroutine(MessageType.Her, dialogLibrary.waitAnswers.GetRandom()));
        StartCoroutine(WaitingResponse());
    }

    private void Continue()
    {
        blockMessages = false;
    }
}
