﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInputManager : MonoBehaviour
{
    public Text inputField;
    public Outline outline;

    public bool canType = false;
    private string sentenceToType;
    private int pointer = 0;

    void Update()
    {
        outline.enabled = canType;
        if (!canType) return;
        if (Input.anyKey && !Input.GetMouseButton(0) && !Input.GetMouseButton(1))
        {
            
            pointer++;
            inputField.text = sentenceToType.Substring(0, pointer);
            inputField.GetComponent<RectTransform>().sizeDelta = new Vector2(
                inputField.GetComponent<RectTransform>().sizeDelta.x, inputField.preferredHeight);
            if (pointer == sentenceToType.Length)
            {
                canType = false;
                GameManager.instance.SendMessage(MessageType.His, sentenceToType);
                GameManager.instance.messageManager.Wait(1.5f);
                inputField.text = "";
            }
        }
    }

    public void LoadSentence(string sentence)
    {
        sentenceToType = sentence;
        canType = true;
        pointer = 0;
    }
}
