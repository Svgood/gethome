﻿using System.Collections;
using System.Collections.Generic;
using Core.Managers;
using SoundSystem;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class EpilogueManager : MonoBehaviour
{
    public Image blackScreen;

    public AudioClip alienScream;

    public Text text;
    public Text dialogueText;
    public Text authorsText;

    public bool isEndGame = false;
    private bool fullyEnded = false;

    public List<Image> stateImages;
    public List<Sprite> morphImages;

    private DialogLibrary dialogLibrary;

    private string epilogueText = "Epilogue";
    private string startText = "I Remember";
    private string finalCitat = "Дом это место, где ты не чувствуешь себя чужим.";

    private int state = 0;

    void Start()
    {
        dialogLibrary = new DialogLibrary();
        StartGame();
        //StartCoroutine(LoadDialogueText(dialogLibrary.epilogueMonologue[state]));
        //StartGame();
    }

    void Update()
    {
        if (!isEndGame) return;
        if (fullyEnded) return;
        if (Input.anyKeyDown)
        {
            StopAllCoroutines();
            state++;
            if (state >= dialogLibrary.epilogueMonologue.Count)
            {
                StartEndGame();
                return;
            }

            if (state > 0 && state < stateImages.Count)
            {
                StartCoroutine(ChangeState());
            }

            StartCoroutine(LoadDialogueText(dialogLibrary.epilogueMonologue[state]));

        }
    }

    public void StartGame()
    {
        if (blackScreen.gameObject.activeSelf)
        {
            StartCoroutine(Prologue(startText, 0));
        }
        else
        {
            GameManager.instance.isGameStarted = true;
        }

    }

    public void StartEpilogue()
    {
        StartCoroutine(Prologue(epilogueText, 3));
    }

    public void StartEndGame()
    {
        if (fullyEnded) return;
        fullyEnded = true;
        StartCoroutine(EndGame());
    }

    private IEnumerator ChangeState()
    {
        while (stateImages[state].color.a < 0.99f)
        {
            stateImages[state].color = Color.Lerp(stateImages[state].color, Color.white, 6 * Time.deltaTime);
            stateImages[state-1].color = Color.Lerp(stateImages[state-1].color, new Color(0,0,0,0), 6 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator LoadDialogueText(string txt)
    {
        dialogueText.text = "";
        int pointer = 0;
        while (dialogueText.text != txt)
        {
            pointer++;
            if (pointer > txt.Length) break;
            dialogueText.text = txt.Substring(0, pointer);
            yield return new WaitForSeconds(0.03f);
        }
    }

    private IEnumerator EndGame()
    {
        SoundManager.instance.SpawnAudioSource(alienScream);
        foreach (var sprite in morphImages)
        {
            blackScreen.color = Color.black;
            stateImages[stateImages.Count - 1].sprite = sprite;
            yield return new WaitForSeconds(0.2f);
            blackScreen.color = new Color(0,0,0,0);
            yield return new WaitForSeconds(0.2f);
        }
        blackScreen.color = Color.black;

        int pointer = 0;
        while (text.text != finalCitat)
        {
            pointer++;
            if (pointer > finalCitat.Length) break;
            text.text = finalCitat.Substring(0, pointer);
            yield return new WaitForSeconds(0.02f);
        }

        yield return new WaitForSeconds(3f);

        while (text.text != "")
        {
            pointer--;
            if (pointer < 0) break;
            text.text = finalCitat.Substring(0, pointer);
            yield return new WaitForSeconds(0.02f);
        }

        yield return new WaitForSeconds(0.5f);

        authorsText.gameObject.SetActive(true);
    }

    private IEnumerator Prologue(string txt, float waitDuration)
    {
        yield return new WaitForSeconds(waitDuration);

        while (blackScreen.color.a < 0.99f)
        {
            blackScreen.color = Color.Lerp(blackScreen.color, Color.black, 6 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        int pointer = 0;
        while (text.text != txt)
        {
            pointer++;
            if (pointer > txt.Length) break;
            text.text = txt.Substring(0, pointer);
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(3f);

        if (txt == epilogueText)
        {
            GameManager.instance.epilogue.gameObject.SetActive(true);
        }

        while (text.text != "")
        {
            pointer--;
            if (pointer < 0) break;
            text.text = txt.Substring(0, pointer);
            yield return new WaitForSeconds(0.1f);
        }

        while (blackScreen.color.a > 0.01f)
        {
            blackScreen.color = Color.Lerp(blackScreen.color, new Color(0,0,0,0), 4 * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        GameManager.instance.isGameStarted = true;

        if (txt == epilogueText)
        {
            isEndGame = true;
            StartCoroutine(LoadDialogueText(dialogLibrary.epilogueMonologue[0]));
        }
    }

}
