﻿using Core;
using System.Collections;
using System.Collections.Generic;
using Core.Items;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CursorManager : MonoBehaviour
{
    public Texture2D standart;
    public Texture2D highlited;
    public Texture2D gear;

    protected List<RaycastResult> raycastTargets = new List<RaycastResult>();

    void Start()
    {
        SetCursor(standart);
    }

    void Update()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        EventSystem.current.RaycastAll(eventData, raycastTargets);
        foreach (var target in raycastTargets)
        {
            if (target.gameObject.GetComponent<Button>() != null)
            {
                SetCursor(highlited);
                return;
            }

            if (target.gameObject.GetComponent<Draggable>() != null)
            {
                SetCursor(highlited);
                return;
            }

            if (target.gameObject.GetComponent<Box>() != null)
            {
                SetCursor(gear);
                return;
            }
        }

        SetCursor(standart);
    }

    public void SetCursor(Texture2D texture)
    {
        Cursor.SetCursor(texture, Vector2.zero,CursorMode.Auto);
    }


}
