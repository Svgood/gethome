﻿using System;
using UnityEngine;

namespace Core
{
    [Serializable]
    public class NewsPaper
    {
        public Sprite newsImage;
        public string newsTitle;
        public string newsDescription;
    }
}
